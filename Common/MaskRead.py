import geopandas as gpd
import sys
import pandas as pd

def read_masks(path_masks, design_label):
    # Read all masks

    print("Read individual masks")
    masks = []
    mask_paths = path_masks.glob(design_label + "*.geojson")
    for i, image_mask in enumerate(mask_paths):
        # Read image mask as geopandas
        gpd_mask = gpd.read_file(str(image_mask))
        masks.append(gpd_mask)
        sys.stdout.write(".")
        sys.stdout.flush()
    gpd_masks = pd.concat(masks, axis=0, ignore_index=True)

    return(gpd_masks)