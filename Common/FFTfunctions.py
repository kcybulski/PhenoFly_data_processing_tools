# @Author: Simon Treier

import numpy as np
from scipy import ndimage

def generate_filter_ellipsoid(GSD, Min_SD, Max_SD, DimPlot, shape, rows, cols):
    X = np.arange(-DimPlot, DimPlot, 1)
    Y = np.arange(-DimPlot, DimPlot, 1)
    X, Y = np.meshgrid(X, Y)

    Y_Dim = rows * GSD
    X_Dim = cols * GSD

    Center_SD = 2 * Max_SD

    El_X_Center = int(round(X_Dim / (2 * Center_SD)))
    El_Y_Center = int(round(Y_Dim / (2 * Center_SD)))

    El_X_min = int(round(X_Dim / (2 * Max_SD)))
    El_X_max = int(round(X_Dim / (2 * Min_SD)))
    El_Y_min = int(round(Y_Dim / (2 * Max_SD)))
    El_Y_max = int(round(Y_Dim / (2 * Min_SD)))

    Eli_center = ((X ** 2) / El_X_Center ** 2) + ((Y ** 2) / El_Y_Center ** 2)
    Eli_inner = ((X ** 2) / El_X_min ** 2) + ((Y ** 2) / El_Y_min ** 2)
    Eli_outer = ((X ** 2) / El_X_max ** 2) + ((Y ** 2) / El_Y_max ** 2)

    #print('El_X_min: ', El_X_min, ' El_X_max: ', El_X_max, ' El_Y_min: ', El_Y_min, ' El_Y_max: ', El_Y_max)

    TEli = np.where(Eli_center <= 1, 1, 0)
    TEli = np.where(Eli_inner >= 1, 1, TEli)
    TEli = np.where(Eli_outer >= 1, 0, TEli)

    TRows, TCols = TEli.shape
    TCropRows, TCropCols = shape

    Tcrop_eli = TEli[np.int((TRows / 2) - (TCropRows / 2)): np.int((TRows / 2) + (TCropRows / 2)),
                np.int((TCols / 2) - (TCropCols / 2)): np.int((TCols / 2) + (TCropCols / 2))]

    TEliInv = 1 - Tcrop_eli

    Tcrop_eli = Tcrop_eli * 255
    Tcrop_eli = ndimage.gaussian_filter(Tcrop_eli, sigma=(3, 3))
    Tcrop_eli = Tcrop_eli / 255
    Tcrop_eli = Tcrop_eli.astype(float)

    return (TEli, TEliInv, Tcrop_eli, X, Y)

def fft_forward_back(band, Tcrop_eli):
    #fgauss = ndimage.gaussian_filter(band, sigma=(1, 1))
    f = np.fft.fft2(band)
    fshift = np.fft.fftshift(f)
    TImg = Tcrop_eli * fshift
    f_ishift = np.fft.ifftshift(TImg)
    img_back = np.fft.ifft2(f_ishift)
    img_back = np.abs(img_back)
    img_back = ndimage.median_filter(img_back, 4)
    img_back = img_back - np.min(img_back)                          # Avoid negative values
    img_back = np.divide(img_back,np.max(img_back))                 # Normalize range to 0 - 1
    img_back = np.multiply(img_back,(np.max(band)-np.min(band)))    # Normalize to original range
    img_back = img_back + np.min(band)                              # Add original min values

    return(img_back)
