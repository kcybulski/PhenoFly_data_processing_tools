import geopandas as gpd
import pandas as pd
import numpy as np
import cv2
import matplotlib as mpl
import imageio
from pathlib import Path
import shutil
import sys

from Common import MaskRead
import time

def affine_transform_images(path_images, path_masks, path_transformed, nx, ny, design_label, offset= 0,
                            image_extension= "tif"):

    print("create", path_transformed)
    path_transformed.mkdir(exist_ok=True)

    # Read all masks
    print("read masks")
    gpd_masks = MaskRead.read_masks(path_masks, design_label)

    gpd_masks_soil = gpd_masks.loc[gpd_masks['type'] == 'soil']
    gpd_masks_soil_nadir = gpd_masks_soil.loc[gpd_masks_soil.groupby('plot_label').zenith_angle.idxmax()]

    gpd_masks_soil_nadir_images = gpd_masks_soil_nadir.groupby('image')

    print("Process images")
    for image, image_group in gpd_masks_soil_nadir_images:
        print("Image: ", image)
        path_image = path_images / (image + '.' + image_extension)
        if path_image.exists():

            for i in range(100):
                try:
                    image = imageio.imread(str(path_image))
                    break
                except ValueError:
                    print("Fail to load", path_image, "second try in 10 secs")
                    time.sleep(10)
                    continue

            sys.stdout.write("Sample plots: ")
            sys.stdout.flush()
            for i, sample in image_group.iterrows():
                sys.stdout.write(sample['plot_label'] + "-")

                plot_polygon = sample.at['geometry']
                x, y = plot_polygon.exterior.coords.xy
                plot_edges_image_coords = np.float32(np.stack([x, y], axis=1))
                # Get affine transform matrix to transform from 0:1, 0:1 space to image coords
                plot_edges_normalized_coords = np.float32([[0,0], [0, ny], [nx, ny], [nx, 0]])
                M_normalized_to_image_coords = cv2.getAffineTransform(plot_edges_image_coords[0:3, :],
                                                                      plot_edges_normalized_coords[0:3, :]
                                                                      )

                dst = cv2.warpAffine(image, M_normalized_to_image_coords, (nx, ny))

                imageio.imwrite(path_transformed / (sample['plot_label'] + ".tif"), dst)
            sys.stdout.flush()
            print(".... done")