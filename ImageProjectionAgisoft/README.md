&copy; Lukas Roth, Group of crop science, ETH Zurich, Part of: [Phenofly UAS data processing tools](../README.md)


# Image mask generation (Standalone Agisoft Script)
## Data Preparation

Basic requirements are

- Agisoft project with **aligned cameras** and a generated (or imported) **DEM** in workspace
- Plot polygon file of your experimental design as **geojson** with the attribute `plot_label`
- Both Agisoft project and geojson need to be in the same coordinate system (any ***metric*** system should work)

Sample Agisoft project:

![Agisoft project](doc/agisoft_project.png)

Sample geojson:
```python
{"type": "FeatureCollection", 
 "features": [{
    "type": "Feature", 
    "properties": {"plot_label": "FPWW0240067"}, 
    "geometry": {
      "type": "Polygon", 
      "coordinates": [
        [[2693786.207215171, 1256288.427557322], 
        [2693790.4785625925, 1256285.030282317], 
        [2693791.407652783, 1256286.1984147034], 
        [2693787.1363053625, 1256289.5956897107], 
        [2693786.207215171, 1256288.427557322]]]
        }
     }, 
  ...
  }

```
  


## Agisoft Metascan Preparation 
You need to add pandas and numpy to the Agisoft Python installation. To do so, run the following line 
in a windows comand window (may require admin rights):

`"C:\Program Files\Agisoft\Metashape Pro\python\python.exe" -m pip install pandas==0.23 numpy`

## Generate image masks

1. Download the python script [Agisoft_standalone_project_points.py](Agisoft_standalone_project_points.py)
2. Open Agisoft Metascan
3. Select "Tools" -> "Run Script..." and run the downloaded script
4. The menu "ETHZ CS Plugin" appears: [cs menu](doc/cs_menu.png)
5. Select "ETHZ CS Plugin" -> "Generate image masks"
6. Select the geojson file with the plot polygons that you prepared
7. Select an empty folder where the masks will be saved
8. Decide whether to use image coordinates (origin is in top left corner, usefull for processing in e.g. Python) or in bottom left corner (usefull for processing in GIS, e.g. QGIS)
9. RUN

## Check results

In the selected folder, one image mask per origin image were generated.

If choosing reverse y coordinates, you may check the accuracy of the image masks in e.g. QGIS by drag-and-drop both origin image and generated plot mask:
![Maks](doc/mask_gis_style.png)

# FAQ

## Error: "Error: float() argument must be a string or a number, not 'list'"
Please check if your geojson has single polygons. 
If it is multipolygon the script will not work. 
To transform from multi- to singlepoygon follow: https://gis.stackexchange.com/questions/64214/saving-of-multi-polygon-shapefile-into-a-individual-polygon-shapefiles




