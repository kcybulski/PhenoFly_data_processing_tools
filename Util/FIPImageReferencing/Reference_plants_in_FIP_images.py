import matplotlib as mpl
mpl.use('Qt5Agg')
from matplotlib import pyplot as plt, gridspec
from matplotlib.path import Path as GeoPath
import cv2

import geopandas as gpd
import numpy as np
import pandas as pd
import imageio


from pathlib import Path
import matplotlib.patches as patches

input_directory = Path('_TestData/canon_EOS_5D_II/20190222')

plant_counts = pd.read_csv(input_directory / 'plant_counts.csv')

# Campaigns
number_of_campaigns = 1
campaign_paths = [input_directory]

# Grab masks of main campaign
main_campaign_list = (campaign_paths[0] / 'masks').glob("*.geojson")

# Plot is interactive
plt.interactive(True)

# List for plot elements
axs = [None for a in range(number_of_campaigns)]


# Init common variables
plant_coords = []
current_masks = []
current_images = []
current_bboxs = []
current_transform_matrixes_I2N = []
current_transform_matrixes_N2I = []

plot_label = None


def init_campaign():
    global  plot_label
    # Reset plant coords
    del plant_coords[:]
    del current_masks[:]
    del current_images[:]
    del current_bboxs[:]
    del current_transform_matrixes_I2N[:]
    del current_transform_matrixes_N2I[:]

    plt.clf()

    # Read main campaign mask to get plot label
    main_mask_path = next(main_campaign_list)
    main_mask = gpd.read_file(str(main_mask_path))
    plot_label = main_mask.iloc[0].at['image']

    plt.title(plot_label + " (Mark: Right mouse, Delete: Middle mouse, Next image: n)")

    # load coordinates file if existing
    try:
        plant_coords.extend(pd.read_csv(campaign_paths[0] / (plot_label + "_plant_coords.csv")).to_dict('records'))
    except:
        print("no coordinates file found")

    for campaign_path in campaign_paths:
        path = campaign_path / 'masks' / ('_' + plot_label + '.geojson')

        # Mask
        mask = gpd.read_file(str(path))
        mask['plot_group_label'] = mask['plot_label'].str[:11]
        current_masks.append(mask)

        # Image
        image = imageio.imread(campaign_path / 'previews' / (plot_label + '.tif'))
        current_images.append(image)

        # Bounding box
        bbox = [
            int(np.min(mask.bounds[['minx']], axis=0)),
            int(np.max(mask.bounds[['maxx']], axis=0)),
            int(np.min(mask.bounds[['miny']], axis=0)),
            int(np.max(mask.bounds[['maxy']], axis=0))
        ]
        current_bboxs.append(bbox)

        # Transform matrixes
        main_mask = mask[mask.plot_label.str.len() == 11].iloc[0]
        plot_polygon = main_mask.at['geometry']
        x_, y_ = plot_polygon.exterior.coords.xy
        plot_edges_image_coords = np.float32(np.stack([x_, y_], axis=1))
        # Get affine transform matrix to transform from image space in normed plot scape (0:1, 0:1)
        plot_edges_normalized_coords = np.float32([[0, 0], [0, 1], [1, 1], [1, 0]])

        M_image_to_norm = cv2.getAffineTransform(plot_edges_image_coords[0:3, :],
                                                 plot_edges_normalized_coords[0:3, :])

        M_norm_to_image = cv2.getAffineTransform(plot_edges_normalized_coords[0:3, :],
                                                 plot_edges_image_coords[0:3, :])

        current_transform_matrixes_I2N.append(M_image_to_norm)
        current_transform_matrixes_N2I.append(M_norm_to_image)

    # Draw patches
    for i in range(number_of_campaigns):

        axs[i] = fig.add_subplot(gs[i])
        axs[i].imshow(current_images[i])
        axs[i].set_xlim(current_bboxs[i][0], current_bboxs[i][1])
        axs[i].set_ylim(current_bboxs[i][2], current_bboxs[i][3])

        for j, mask in current_masks[i].iterrows():
            path = GeoPath([a for a in mask.geometry.exterior.coords])

            patch = patches.PathPatch(path, facecolor='white', edgecolor='white', fill=False, lw=1)
            axs[i].add_patch(patch)

            plot_data = plant_counts[plant_counts['plot_label'] == mask['plot_label']]
            if len(plot_data)>0:
                txt = "%.0f" % float(plant_counts[plant_counts['plot_label'] == mask['plot_label']]['value'])
                axs[i].text(mask.geometry.exterior.coords[0][0] + 30, mask.geometry.exterior.coords[0][1]+15,
                                 txt, color='white', fontsize=10,
                                 horizontalalignment='center', verticalalignment='top')

# Drawing functions
def draw_points():

    # Remove allpoints from graph
    for i in range(number_of_campaigns):
        del axs[i].collections[:]

    fig.canvas.draw()
    if len(plant_coords) > 0:

        plant_coords_arr = np.array([[a['x'], a['y']] for a in plant_coords], dtype=np.float32)

        for i in range(number_of_campaigns):
            plant_pos = np.dot(np.c_[plant_coords_arr, np.ones(plant_coords_arr.shape[0])],
                                      current_transform_matrixes_N2I[i].T)

            axs[i].scatter('x', 'y', data=pd.DataFrame(plant_pos, columns=("x", "y")), marker='+',
                                color='white')

# In click: add or delete training points
def onclick(event):
    # Coordinates of click
    tb = plt.get_current_fig_manager().toolbar

    x = event.xdata
    y = event.ydata
    print(x, y)
    i = 0
    for axis in fig.axes:
        if axis == event.inaxes:
            break
        i+=1
    i-=1
    print("Fig", i)

    # Transform to wold coords
    # Get postion of plot edges on image
    M = current_transform_matrixes_I2N[i]
    print("fig", i)

    clicked_pos = np.array([[x, y]])
    clicked_pos_norm = np.dot(np.c_[clicked_pos, np.ones(clicked_pos.shape[0])],
                              M.T)

    if tb.mode == '':
        if event.button == 3:
            plant_coords.append({'x': clicked_pos_norm[0][0], 'y': clicked_pos_norm[0][1]})
        elif event.button == 2:
            df_plant_coords = pd.DataFrame(plant_coords)
            diff = np.power(df_plant_coords['x'] - clicked_pos_norm[0][0], 2) * 20 * 4 + np.power(df_plant_coords['y'] - clicked_pos_norm[0][1], 2)
            min_id = np.argmin(diff)
            print(min_id)
            del plant_coords[min_id]

        # Redraw graph
        draw_points()

# On button pressed: load new images or train algorithm
def onkey(event):

    if event.key == 'n':
        pd.DataFrame(plant_coords).to_csv(campaign_paths[0] / (plot_label + "_plant_coords.csv"))
        init_campaign()
        draw_points()


if __name__ == "__main__":
    # Generate figure
    fig = plt.figure(figsize=(10, 4))

    fig.tight_layout()
    gs = gridspec.GridSpec(number_of_campaigns, 1)

    fig.canvas.mpl_connect('button_press_event', onclick)
    fig.canvas.mpl_connect('key_press_event', onkey)

    init_campaign()
    draw_points()

    plt.pause(0)



