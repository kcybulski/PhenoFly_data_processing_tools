&copy; Lukas Roth, Group of crop science, ETH Zurich, Part of: [Phenofly UAS data processing tools](../README.md)

# Early growth trait extraction

Sample scripts to extract plant count, tiller count and beginning of stem elongation values.

## Background
Based on:

Roth, Camenzind, Aasen, Kronenberg, Barendregt, Camp, Walter, Kirchgessner, Hund (2020). Repeated Multiview Imaging for Estimating Seedling Tiller Counts of Wheat Genotypes Using Drones. Plant Phenomics, 2020(3729715). https://doi.org/10.34133/2020/3729715

## Example 

Run ```EarlyGrowthTraitExtraction/_Demo_.py```

Prerequisit: 
1. Image masks generated (see [Image mask generation (Standalone Agisoft Script)](../ImageProjectionAgisoft/README.md))
2. Images segmented in plant and soin (see [Image segmentation with Random Forest](../ActiveLearningSegmentation/README.md))
3. Multi-view images gerated (see [Multi-view image generation](../MultiViewImage/README.md))

The campaign folder should therefore contain following subfolders:
- ```image_masks```
- ```segmentation```
- ```GC_AC```

It will generate the following folders:
- ```trait_csvs```: CSVs with extracted trait values per plot



