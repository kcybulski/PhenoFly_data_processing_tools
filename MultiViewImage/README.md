&copy; Lukas Roth, Group of crop science, ETH Zurich, Part of: [Phenofly UAS data processing tools](../README.md)

# Multi-view image generation

Sample scripts to generate segmented multi-view images

## Background
Based on:

Roth et al. 2019 (unpublished)

## Example 

Run ```MultiViewImage/_Demo_.py```

Prerequisit: 
1. Image masks generated (see [Image mask generation (Standalone Agisoft Script)](../ImageProjectionAgisoft/README.md))
2. Images segmented in plant and soil (see [Image segmentation with Random Forest](../ActiveLearningSegmentation/README.md))

The campaign folder should therefore contain following subfolders:
- ```image_masks```
- ```segmentation```

It will generate the following folders:
- ```GC_AC```: Genereated segmented multi-view images
- ```GC_AC/*_canopy_coverages.csv```: Extracted canopy cover value per image and plot



