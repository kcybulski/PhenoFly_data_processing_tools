import os
from pathlib import Path

from MultiViewImage import MultiViewImageGenerator

# Paths and settings for sample campaign
########################################

base_dir = Path(os.path.join(os.path.dirname(os.path.realpath(__file__))))
design_label = "FPWW025_lot2"
path_files = base_dir / ".." / "_TestData" / "sonyA9" / "20190402" / "28m_M600P"

# Ground sampling distance
GSD = 0.003  # m
# Buffer to apply before sampling
buffer = round(-0.3 / GSD)  # 0.3 m buffer
# GSD of multi-view images
GSD_output = 0.001  # m
# Size of plot in pixel
ny, nx = (round((2 - (2 * 0.3)) / GSD_output), round((1.5 - (2 * 0.3)) / GSD_output))

# Generate multi-view images
############################

MultiViewImageGenerator.process_multiview_images_generation(
    path_files,
    design_label,
    nx=nx, ny=ny,
    subplots=None,
    buffer=buffer)

print("Segmented multi view images generated in", path_files / "GC_AC")
