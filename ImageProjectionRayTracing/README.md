&copy; Lukas Roth, Group of crop science, ETH Zurich, Part of: [Phenofly UAS data processing tools](../README.md)
# Image mask generation (Ray tracing to DTM, deprecated)

Sample scripts to project images on a digital terrain model (DTM), followed by plot-based sample extraction.

**Attention**: A newer version as standalone Agisoft Metashape Plugin is available: [Image mask generation (Standalone Agisoft Metashape Script)](ImageProjectionAgisoft/README.md)

## Background
Based on:

Roth, Aasen, Walter, Liebisch 2018: Extracting leaf area index using viewing geometry effects—A new perspective on high-resolution unmanned aerial system photography, ISPRS Journal of Photogrammetry and Remote Sensing.
https://doi.org/10.1016/j.isprsjprs.2018.04.012

## Example 

See ```ImageProjectionSampling/_Demo_.py```

Input:
- Agisoft camera position file (_TestData/sonyDX100II/camera_position_RGB.txt)
- Digital terrain model in STL file format (_TestData/sonyDX100II/DTM.stl)
- Folder with images (_TestData/sonyDX100II/raw/)
- Polygon sampling layer in geoJSON format (_TestData/sonyDX100II/samples_plots.geojson)

Output:
- Per-image coordinate and viewpoint information (_Demo_output/dg/)
- Image boundaries in wold coordinates as geoJSON (_Demo_output/dg/DG_field_of_views.geojson)
- Extracted plot-based image parts (_Demo_output/samples/)
- Viewpoint of each extracted sample (_Demo_output/samples/viewpoint.csv)
