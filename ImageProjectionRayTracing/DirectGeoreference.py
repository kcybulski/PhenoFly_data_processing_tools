##############################################################################
#
# Project: PhenoFly UAS data processing tools
# File: Direct georeference images based on camera position and Waypoint_flight
#
# Author: Lukas Roth (lukas.roth@usys.ethz.ch)
#
# Copyright (C) 2018  ETH Zürich, Lukas Roth (lukas.roth@usys.ethz.ch)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
##############################################################################

# Input:
# - Camera positions
# - Digital terrain model (DTM) in STL format
# - Camera/sensor parameter (sensor dimension, focal length, sensor resolution)
# - Number of interpolation points in x and y
# - EPSG coordinate system (only cartesian systems supported)
#
# Output:
# - Longitude/Latitude coordinates TIFF per image
# - Zenith/Azimuth angle TIFF per image
# - geoJSON showing shapes of all images
#

# Dependencies
import csv
import shutil
import sys
import gdal
from stl import mesh
import numpy as np
import os
import argparse
import time
import math
import geojson
import multiprocessing

# Specific imports
from multiprocessing import Process, Manager
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from geojson import Polygon, Feature, FeatureCollection

# Own imports
from Common.GISfunctions import ray_plane_intersect, point_in_polygon, find_bounding_box_mesh, get_triangle_normale, \
    rotation_matrix, create_nband_GeoTiff

################################################################################
# Helper functions
################################################################################

# Axis for rotations
x_axis = [1, 0, 0]
y_axis = [0, 1, 0]
z_axis = [0, 0, 1]

# Max value in an uint16
uint16_max = 65536


def ray_tracing_worker(work_queue, result_queue, plane_normales, plane_points):
    """ Worker to process ray tracing for pixel rays

    :param work_queue: Work queue with pixel ray jobs
    :param result_queue: Queue to write found intersects to
    :param plane_normales: Normale of planes representing the Waypoint_flight
    :param plane_points: Points of planes representing the Waypoint_flight
    :return: None
    """

    # Serve on pool of jobs
    for job in iter(work_queue.get, 'STOP'):
        pixel_nr = job['pixel_nr']
        ray_vector = job['ray_vector']
        ray_point = job['ray_point']
        bounding_box = job['bounding_box']
        verbose = job['verbose']
        limit = job['limit']
        partition_size = job['partition_size']

        # Intersect specific pixel ray with Waypoint_flight planes
        intersects = process_ray_tracing_job(pixel_nr, plane_normales, plane_points, ray_vector, ray_point,
                                             bounding_box, verbose, limit=limit, partition_size=partition_size)
        # Put result in result queue
        result_queue.put([pixel_nr, intersects])


def process_ray_tracing_job(pixel_nr, plane_normales, plane_points, ray_vector, ray_point, bounding_box, verbose=False,
                            partition_size=200, limit=10000):
    """Accelerated ray tracer: First approximate intersect, then perform exact ray trace using planes with a high
    intersection probability

    :param pixel_nr: Job/pixel number
    :param plane_normales: Normales of planes representing the Waypoint_flight
    :param plane_points: Points of planes representing the Waypoint_flight
    :param ray_vector: Vector of pixel ray
    :param ray_point: Point on pixel ray
    :param bounding_box: Bounding box for approximation step
    :param verbose: Verbose output - provides information about time spend for specific operations
    :param partition_size: Size of first partition used to accelerate calculation run. Large value increase
    probability of a hit, but reduce speed.
    :param limit: Number of planes the ray should be intersected with before giving up and report no valid intersection
    :return: None
    """

    if verbose: t_start = time.time()

    # Initialize approximation Waypoint_flight
    # Create two triangles based on edges of the bounding box to approximate the Waypoint_flight normales
    dtm_approx_triangle_normale = get_triangle_normale([bounding_box[0], bounding_box[1], bounding_box[2]])
    dtm_approx_triangle_point = bounding_box[0]
    # Estimate intersection of ray with approximation Waypoint_flight triangles
    # Intersect ray with two triangles
    approximation_point = np.array(
        ray_plane_intersect(ray_point, ray_vector, dtm_approx_triangle_point, dtm_approx_triangle_normale),
        dtype=np.float64)
    # Log time for approximation
    if verbose: t_approximation = round(time.time() - t_start, 2)
    # If no approximation point found then point is outside bounding box, return NAN
    if approximation_point is None:
        print("WARNING: Pixel {0} has no apprimated intersection in bounding box. Skip.".format(pixel_nr), flush=True)
        return [np.nan, np.nan, np.nan]

    # Calculate distance of planes to approximated point
    # Simple euclidian distance
    delta_plane_to_intersect = np.array(plane_points[:, [0, 1]], dtype=np.float64) - approximation_point[[0, 1]]
    euclidian_distance_plane_to_intersect = (delta_plane_to_intersect[:, 0] ** 2) + (
                delta_plane_to_intersect[:, 1] ** 2)
    # Sort planes by distance
    # Create partitioned index, x closest planes in first partition
    closest_planes_index = np.argpartition(euclidian_distance_plane_to_intersect, partition_size - 1)[0:partition_size]
    # Recalculate intersection based on real Waypoint_flight
    approximation_point_recalc = ray_plane_intersect(ray_point, ray_vector, plane_points[closest_planes_index[0]][0:3],
                                                     plane_normales[closest_planes_index[0]])
    approximation_point = np.array(approximation_point_recalc, dtype=np.float64) if (
                approximation_point_recalc is not None) else approximation_point

    delta_plane_to_intersect = np.array(plane_points[:, [0, 1]], dtype=np.float64) - approximation_point[[0, 1]]
    euclidian_distance_plane_to_intersect = (delta_plane_to_intersect[:, 0] ** 2) + (
            delta_plane_to_intersect[:, 1] ** 2)
    closest_planes_index = np.argpartition(euclidian_distance_plane_to_intersect, partition_size - 1)[0:partition_size]
    # Log time for distance calculation and partitioning
    if verbose: t_add = round(time.time() - t_start - t_approximation, 2)

    # Intersect closest x planes with ray
    i, intersect = process_ray_planes_intersect(closest_planes_index, plane_normales, plane_points, ray_vector,
                                                ray_point)
    if intersect is None:
        # No intersect with x closest planes. Use other planes too, up to the limit
        if verbose:
            print("WARNING: Pixel {0} needs full run. Think about increasing partitioning size (currently {1})".format(
                pixel_nr, partition_size), flush=True)
        other_planes_index = np.argsort(euclidian_distance_plane_to_intersect)[partition_size:limit]
        i, intersect = process_ray_planes_intersect(other_planes_index, plane_normales, plane_points, ray_vector,
                                                    ray_point)
    # Log timespan until found
    if verbose:
        t_found = round(time.time() - t_start - t_approximation - t_add, 2)

    # Analyze run
    if intersect is not None:
        # Intersect found, print log
        if verbose:
            print("Pixel {0} calculated, {1} loops. Times: Init normal: {2}, Sort: {3}, Intersect: {4}".format(pixel_nr,
                                                                                                               i,
                                                                                                               t_approximation,
                                                                                                               t_add,
                                                                                                               t_found),
                  flush=True)
        elif pixel_nr % 50 == 0:
            print("{} done".format(pixel_nr), flush=True)
        # Return result
        return intersect
    else:
        # No intersect found, print log
        if verbose:
            t_found = round(time.time() - t_start - t_approximation - t_add, 2)
            print(
                "Pixel {0} not found, {1} loops. Times: Init normal: {2}, Sort: {3}, Intersect: {4}".format(pixel_nr, i,
                                                                                                            t_approximation,
                                                                                                            t_add,
                                                                                                            t_found),
                flush=True)
        elif pixel_nr % 50 == 0:
            print("{} done".format(pixel_nr), flush=True)
        # Return NAN array
        return [np.nan, np.nan, np.nan]


def process_ray_planes_intersect(index_list, plane_normales, plane_points, ray_vector, ray_point):
    """Perform ray tracing for multiple planes in order of a given index

    :param index_list: Order of planes to process
    :param plane_normales: Normales of planes representing the Waypoint_flight
    :param plane_points: Points of planes representing the Waypoint_flight
    :param ray_vector: Vector of pixel ray
    :param ray_point: Point on pixel ray
    :return: Intersection
    """

    # Follow the order given in the index
    for i, plane_index in enumerate(index_list):
        # Get plane normale and point
        plane_normal = plane_normales[plane_index]
        plane_point = plane_points[plane_index, [0, 1, 2]]

        # Intersect line with plane
        intersect_point = ray_plane_intersect(ray_point, ray_vector, plane_point, plane_normal)
        if (intersect_point is not None):
            # Test if found intersection is in plane or outside
            p1_x, p1_y, p2_x, p2_y, p3_x, p3_y = plane_points[plane_index, [0, 1, 3, 4, 6, 7]].tolist()
            hit = point_in_polygon((intersect_point[0], intersect_point[1]), [(p1_x, p1_y), (p2_x, p2_y), (p3_x, p3_y)])
            if hit:
                # If intersection is part of plane: True intersection, skip loop and return coordinates
                return i, intersect_point.tolist()
            else:
                # No real intersection, continue
                pass
    return i, None


###main process routine

def process(camera_position_file, DTM_stl_file, no_of_interpolation_points_x, no_of_interpolation_points_y,
            output_directory,
            epsg, sensor_dimension_x, sensor_dimension_y, focal_length, sensor_resolution_x, sensor_resolution_y,
            flight_height,
            cx, cy, k1, verbose=False):
    ground_resolution = (sensor_dimension_x / sensor_resolution_x) * flight_height / focal_length

    # give short summary of arguments
    print(
        '================================================================================\nDirect georeference images based on camera position and Waypoint_flight')
    print('================================================================================')
    print('Paths:\n--------------------------------------------------------------------------------')
    print('Camera position file:', camera_position_file)
    print('Waypoint_flight STL file:', DTM_stl_file)
    print('Output folder:', output_directory)
    print(
        '--------------------------------------------------------------------------------\nSensor:\n--------------------------------------------------------------------------------')
    print('{0} x {1} ({2} x {3} mm), f: {4} mm'.format(sensor_resolution_x, sensor_resolution_y,
                                                       sensor_dimension_x * 1000, sensor_dimension_y * 1000,
                                                       focal_length * 1000))
    print('Ground resolution: {0} mm'.format(round(ground_resolution * 1000, 1)))
    print('================================================================================')

    ###########################
    # Start

    # Prepare output directory, delete content
    shutil.rmtree(output_directory, ignore_errors=True)
    os.mkdir(output_directory)

    # Load Waypoint_flight
    print('Loading Waypoint_flight\n--------------------------------------------------------------------------------')
    DTM_stl = mesh.Mesh.from_file(DTM_stl_file)

    DTM_normales = DTM_stl.normals
    DTM_points = DTM_stl.points

    # Find minimum and maximum values of Waypoint_flight and define bounding box
    minx, maxx, miny, maxy, minz, maxz = find_bounding_box_mesh(DTM_stl)
    range_x, range_y, range_z = (maxx - minx, maxy - miny, maxz - minz)
    print("Global bounding box: X: {0}-{1}, Y: {2}-{3}, Z: {4}-{5}".format(minx, maxx, miny, maxy, minz, maxz))

    # Define offsets to increase precision of float types and reduce resulting file sizes
    offset_x, offset_y, offset_z = (np.floor(x) for x in [minx, miny, minz])
    print("Local bounding box: X: {0}-{1}, Y: {2}-{3}, Z: {4}-{5}".format(0, maxx - offset_x, 0, maxy - offset_y, 0,
                                                                          maxz - offset_z))
    # Create projection area for first ray tracing approximations
    meanz = np.ceil((maxz + minz) / 2)
    projection_plane_approximation = [(minx - offset_x, miny - offset_y, meanz - offset_z),
                                      (minx - offset_x, maxy - offset_y, meanz - offset_z),
                                      (maxx - offset_x, maxy - offset_y, meanz - offset_z),
                                      (maxx - offset_x, miny - offset_y, meanz - offset_z)]

    # Correct Waypoint_flight with offset
    DTM_points_ = DTM_points.reshape(-1, 3)
    DTM_points_[:, 0] -= offset_x
    DTM_points_[:, 1] -= offset_y
    DTM_points_[:, 2] -= offset_z
    DTM_points = DTM_points_.reshape(DTM_points.shape)

    # Initialize sensor
    # Position of pixels used to inpterpolate all other pixels
    sensor_pixels_pos_x = np.round(np.linspace(0, sensor_resolution_x - 1, no_of_interpolation_points_x))

    sensor_pixels_pos_y = np.round(np.linspace(0, sensor_resolution_y - 1, no_of_interpolation_points_y))
    sensor_pixels_pos_xy = np.transpose([np.repeat(sensor_pixels_pos_x, len(sensor_pixels_pos_y)),
                                         np.tile(sensor_pixels_pos_y, len(sensor_pixels_pos_x))])
    # Coordinates of pixels used to interpolate all other pixels
    sensor_pixels_x = np.linspace(-(sensor_dimension_x / 2.0), sensor_dimension_x / 2.0, no_of_interpolation_points_x,
                                  dtype=np.float64)
    sensor_pixels_y = np.linspace(-(sensor_dimension_y / 2.0), sensor_dimension_y / 2.0, no_of_interpolation_points_y,
                                  dtype=np.float64)

    sensor_pixels_xy = np.transpose(
        [np.repeat(sensor_pixels_x, len(sensor_pixels_y)), np.tile(sensor_pixels_y, len(sensor_pixels_x))])

    sensor_pixels_x_dist = sensor_pixels_xy[:, 0] * (
            1 + (sensor_pixels_xy[:, 0] ** 2 + sensor_pixels_xy[:, 1] ** 2) * k1) + cx * (
                                   sensor_dimension_x / sensor_resolution_x)
    sensor_pixels_y_dist = sensor_pixels_xy[:, 1] * (
            1 + (sensor_pixels_xy[:, 0] ** 2 + sensor_pixels_xy[:, 1] ** 2) * k1) - cy * (
                                   sensor_dimension_y / sensor_resolution_y)

    sensor_pixels_pox_xy_dist = np.stack((sensor_pixels_x_dist, sensor_pixels_y_dist), axis=1)

    sensor_pixel_z = np.full((sensor_pixels_xy.shape[0], 1), focal_length, dtype=np.float64)
    sensor_pixels_xyz = np.concatenate((sensor_pixels_pox_xy_dist, sensor_pixel_z), axis=1)
    # Index of pixels at image edges
    sensor_pixels_index_edges = [0, (no_of_interpolation_points_y - 1),
                                 (no_of_interpolation_points_x * no_of_interpolation_points_y - 1), (
                                         no_of_interpolation_points_x * no_of_interpolation_points_y - no_of_interpolation_points_y)]

    # Calculate field of view for all cameras
    # Reset field of view geoJSON file if existing
    open(os.path.join(output_directory, 'DG_field_of_views.geojson'), 'w').close()
    crs = {
        "type": "name",
        "properties": {
            "name": "EPSG:{0}".format(epsg)
        }
    }
    features = []
    # Process all cameras
    with open(camera_position_file, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:

            image_name, image_extension = os.path.splitext(row['image'])

            print('================================================================================\nPROCESSING',
                  image_name, '\n--------------------------------------------------------------------------------')

            # Read in position and orientation parameters, scale to uint32 space
            X, Y, Z = (float(row[index]) for index in ["X", "Y", "Z"])
            X -= offset_x
            Y -= offset_y
            Z -= offset_z
            Omega, Phi, Kappa = (float(row[index]) for index in ["Omega", "Phi", "Kappa"])
            # Convert to radian
            omega_rad, phi_rad, kappa_rad = (math.radians(x) for x in [Omega, Phi, Kappa])

            # Set the coordinates of the focal point
            focal_point = np.array([X, Y, Z], dtype=np.float64)

            # Rotate sensor vectors
            print("Rotate sensor...")
            sensor_pixels_vectors = np.array(
                [np.dot(rotation_matrix(z_axis, kappa_rad), sensor_pixel) for sensor_pixel in sensor_pixels_xyz],
                dtype=np.float64)
            sensor_pixels_vectors = np.array(
                [np.dot(rotation_matrix(y_axis, phi_rad), sensor_pixel) for sensor_pixel in sensor_pixels_vectors],
                dtype=np.float64)
            sensor_pixels_vectors = np.array(
                [np.dot(rotation_matrix(x_axis, omega_rad), sensor_pixel) for sensor_pixel in sensor_pixels_vectors],
                dtype=np.float64)

            # Ray trace edge pixels
            print("Ray trace edge pixels...")
            sensor_edge_pixels_vectors = sensor_pixels_vectors[sensor_pixels_index_edges]

            m = Manager()
            jobs = m.Queue()
            results = m.Queue()
            processes = []

            # Build job queue
            for pixel_nr, vector in enumerate(sensor_edge_pixels_vectors):
                job = dict()
                job['pixel_nr'] = pixel_nr
                job['ray_vector'] = vector
                job['ray_point'] = focal_point
                job['bounding_box'] = projection_plane_approximation
                job['verbose'] = verbose
                job['limit'] = 10000
                job['partition_size'] = 150
                jobs.put(job)

            # Start workers. Save 2 cpu cores for main thread and os. Maximum 4 workers (4 edges).
            for w in range(multiprocessing.cpu_count() - 2 if (multiprocessing.cpu_count() - 2) > 4 else 4):
                p = Process(target=ray_tracing_worker, args=(jobs, results, DTM_normales, DTM_points))
                p.start()
                processes.append(p)
                jobs.put('STOP')

            for p in processes:
                p.join()

            results.put('STOP')

            # Collect results
            image_edge_coords = []
            indexs = []
            for pixel in iter(results.get, 'STOP'):
                image_edge_coords.append(pixel[1])
                indexs.append(pixel[0])
            image_edge_coords = np.array(image_edge_coords, dtype=np.float64)
            image_edge_coords = image_edge_coords[np.argsort(indexs)]

            print("\nImage edges found:\n", image_edge_coords)
            if (np.isnan(image_edge_coords.flat).any()):
                print("WARNING: Image edges missing, have to calculate with approximation plane :(")
                image_edge_coords = projection_plane_approximation
            else:
                # Write geoJSON file with field of view of images
                # Add start point as end point to close polygon
                print("\nSave field of view polygon to geoJSON")
                # Polygon
                corner_polygon = np.append(image_edge_coords, [image_edge_coords[0]], axis=0)
                corner_polygon[:, 0] += offset_x
                corner_polygon[:, 1] += offset_y
                corner_polygon[:, 2] += offset_z
                corner_polygon = Polygon([corner_polygon.tolist()])
                # Feature
                properties = {'image_name': image_name}
                feature = Feature(geometry=corner_polygon, properties=properties)
                features.append(feature)
                # Feature collection with projection coordinate system
                feature_collection = FeatureCollection(features, crs=crs)
                # Append to file
                with open(os.path.join(output_directory, 'DG_field_of_views.geojson'), 'w') as f:
                    geojson.dump(feature_collection, f, sort_keys=True, indent=4)

            # Do the full ray trace run on all sensor pixels that are not interpolated later
            print(
                "\nRay trace all sensor pixels\n--------------------------------------------------------------------------------")
            print('Points to calculate:', sensor_pixels_vectors.shape[0])
            print('Planes to intersect with:', DTM_normales.shape[0])
            print('Calculate intersections, running on', multiprocessing.cpu_count() - 2, 'cores\n')

            m = Manager()
            jobs = m.Queue()
            results = m.Queue()
            processes = []

            # Build job queue
            for pixel_nr, vector in enumerate(sensor_pixels_vectors):
                job = dict()
                job['pixel_nr'] = pixel_nr
                job['ray_vector'] = vector
                job['ray_point'] = focal_point
                job['bounding_box'] = image_edge_coords
                job['verbose'] = verbose
                job['limit'] = 10000
                job['partition_size'] = 150
                jobs.put(job)

            # Start workers. Save 2 cpu cores for main thread and os
            for w in range(multiprocessing.cpu_count() - 2):
                p = Process(target=ray_tracing_worker, args=(jobs, results, DTM_normales, DTM_points))
                p.start()
                processes.append(p)
                jobs.put('STOP')

            for p in processes:
                p.join()

            results.put('STOP')
            print("\n...done. Getting results")

            # Collect results
            pixels = []
            indexs = []
            for pixel in iter(results.get, 'STOP'):
                pixels.append(pixel[1])
                indexs.append(pixel[0])
            sensor_pixel_coords = np.array(pixels, dtype=np.float64)
            sensor_pixel_coords = sensor_pixel_coords[np.argsort(indexs)]

            # Calculate viewing geometry
            print("\nCalculating viewing geometry")
            # azimuth angle
            azimuth_proj = np.arctan2(Y - sensor_pixel_coords[:, 1], X - sensor_pixel_coords[:, 0])
            ground_pixel_angle_azimuth = 2 * np.pi - (azimuth_proj) % (2 * np.pi)
            ground_pixel_angle_zenith = -(np.arctan((Z - sensor_pixel_coords[:, 2]) / np.sqrt(
                (X - sensor_pixel_coords[:, 0]) * (X - sensor_pixel_coords[:, 0]) + (Y - sensor_pixel_coords[:, 1]) * (
                        Y - sensor_pixel_coords[:, 1]))) - np.pi / 2)

            # Interpolating x, y, z coordinates and viewing geometry for all sensor pixels
            print("Interpolating pixels")
            # Generate full grid that contains all sensor pixels
            grid_x, grid_y = np.mgrid[0:sensor_resolution_x, 0:sensor_resolution_y]

            # X coords
            # Position of pixels where the value is known
            sensor_pixels_pos_x = sensor_pixels_pos_xy[np.logical_not(np.isnan(sensor_pixel_coords[:, 0]))]
            # Values of this pixels
            ground_pixel_coords_x = sensor_pixel_coords[np.logical_not(np.isnan(sensor_pixel_coords[:, 0])), 0]
            # Interpolate to full grid
            ground_pixel_coords_x_interpol = griddata(sensor_pixels_pos_x, ground_pixel_coords_x, (grid_x, grid_y),
                                                      method='linear')

            # Y coords
            sensor_pixels_pos_y = sensor_pixels_pos_xy[np.logical_not(np.isnan(sensor_pixel_coords[:, 1]))]
            ground_pixel_coords_y = sensor_pixel_coords[np.logical_not(np.isnan(sensor_pixel_coords[:, 1])), 1]
            ground_pixel_coords_y_interpol = griddata(sensor_pixels_pos_y, ground_pixel_coords_y, (grid_x, grid_y),
                                                      method='linear')

            # Z coords
            sensor_pixels_pos_z = sensor_pixels_pos_xy[np.logical_not(np.isnan(sensor_pixel_coords[:, 2]))]
            ground_pixel_coords_z = sensor_pixel_coords[np.logical_not(np.isnan(sensor_pixel_coords[:, 2])), 2]
            ground_pixel_coords_z_interpol = griddata(sensor_pixels_pos_z, ground_pixel_coords_z, (grid_x, grid_y),
                                                      method='linear')

            # Azimuth
            sensor_pixels_pos_azimuth = sensor_pixels_pos_xy[np.logical_not(np.isnan(ground_pixel_angle_azimuth))]
            ground_pixel_angle_azimuth = ground_pixel_angle_azimuth[
                np.logical_not(np.isnan(ground_pixel_angle_azimuth))]
            ground_pixel_angle_azimuth_interpol = griddata(sensor_pixels_pos_azimuth, ground_pixel_angle_azimuth,
                                                           (grid_x, grid_y), method='linear')

            # Zenith
            sensor_pixels_pos_zenith = sensor_pixels_pos_xy[np.logical_not(np.isnan(ground_pixel_angle_zenith))]
            ground_pixel_angle_zenith = ground_pixel_angle_zenith[np.logical_not(np.isnan(ground_pixel_angle_zenith))]
            ground_pixel_angle_zenith_interpol = griddata(sensor_pixels_pos_zenith, ground_pixel_angle_zenith,
                                                          (grid_x, grid_y), method='linear')

            if verbose:
                # Plot an overview
                print("Create overview file")
                # Switch interactive graph output off
                plt.ioff()
                # Create figures
                plt.figure(figsize=(10, 7))

                plt.subplot(231)
                plt.imshow(np.fliplr(ground_pixel_coords_x_interpol.T))
                plt.title('X coord')
                plt.colorbar(orientation='horizontal')

                plt.subplot(232)
                plt.imshow(np.fliplr(ground_pixel_coords_y_interpol.T))
                plt.title('Y coord')
                plt.colorbar(orientation='horizontal')

                plt.subplot(233)
                plt.imshow(np.fliplr(ground_pixel_coords_z_interpol.T))
                plt.title('Z coord')
                plt.colorbar(orientation='horizontal')

                plt.subplot(234)
                plt.imshow(np.fliplr(ground_pixel_angle_azimuth_interpol.T))
                plt.title('azimuth view angle')
                plt.colorbar(orientation='horizontal')

                plt.subplot(235)
                plt.imshow(np.fliplr(ground_pixel_angle_zenith_interpol.T))
                plt.title('zenith view angle')
                plt.colorbar(orientation='horizontal')

                plt.savefig(os.path.join(output_directory, image_name + '_DG_overview.png'), dpi=80)
                plt.close()

            # Write calcualted matrices to multiband geoTIFF files
            print("\nWrite results to TIFF files")
            # Raise exception if the range of x or y axis is larger than a uint16 band has values
            if (np.ptp(ground_pixel_coords_x_interpol) > uint16_max * ground_resolution):
                raise Exception(
                    "ERROR: uint16 range {0} too small for range of x coordinates {1}".format(uint16_max, np.ptp(
                        ground_pixel_coords_x_interpol)))

            if (np.ptp(ground_pixel_coords_y_interpol) > uint16_max * ground_resolution):
                raise Exception(
                    "ERROR: uint16 range {0} too small for range of y coordinates {1}".format(uint16_max, np.ptp(
                        ground_pixel_coords_y_interpol)))

            # Write files
            create_nband_GeoTiff(file_path=os.path.join(output_directory, image_name + '_DG_coord_xy.tif'),
                                 arrays=[np.array(np.fliplr(ground_pixel_coords_x_interpol.T / ground_resolution)),
                                         np.array(np.fliplr(ground_pixel_coords_y_interpol.T / ground_resolution))],
                                 data_type=gdal.GDT_UInt16,
                                 keys=[{"DG_TYPE": "X", "DG_OFFSET": offset_x, "DG_SCALE": ground_resolution},
                                       {"DG_TYPE": "Y", "DG_OFFSET": offset_y, "DG_SCALE": ground_resolution}])

            create_nband_GeoTiff(file_path=os.path.join(output_directory, image_name + '_DG_viewing_geometry.tif'),
                                 arrays=[np.array(np.rad2deg(np.fliplr(ground_pixel_angle_zenith_interpol.T)) * 2.0,
                                                  dtype=np.byte),
                                         np.array(np.rad2deg(np.fliplr(ground_pixel_angle_azimuth_interpol.T)) / 2.0,
                                                  dtype=np.byte)],
                                 data_type=gdal.GDT_Byte,
                                 keys=[{"DG_TYPE": "zenith", "DG_OFFSET": "0", "DG_SCALE": "0.5"},
                                       {"DG_TYPE": "azimuth", "DG_OFFSET": "0", "DG_SCALE": "2"}])

            print("\nDONE\n")


################################################################################
# Main
################################################################################


def main(argv):
    # Configure command line arguments
    parser = argparse.ArgumentParser(
        description='Direct georeference images based on camera position and Waypoint_flight')

    parser.add_argument("output", help="path to output directory")

    parser.add_argument("-a", "--sensor_size_x", help="sensor size in x (mm)", type=float, required=True)
    parser.add_argument("-b", "--sensor_size_y", help="sensor size in y (mm)", type=float, required=True)
    parser.add_argument("-x", "--sensor_res_x", help="sensor resolution in x (px)", type=int, required=True)
    parser.add_argument("-y", "--sensor_res_y", help="sensor resolution in y (px)", type=int, required=True)
    parser.add_argument("-f", "--focal_lenght", help="focal lenght of lens (mm)", type=float, required=True)
    parser.add_argument("-g", "--flight_height", help="flight height (m)", type=float, required=True)

    parser.add_argument("-d", "--dem", help="path to DEM STL file", required=True)
    parser.add_argument("-c", "--cameras", help="path to camera position file", required=True)
    parser.add_argument("-i", "--interpol_i", help="number of interpolations in sensor x", type=int, default=20)
    parser.add_argument("-j", "--interpol_j", help="number of interpolations in sensor y", type=int, default=20)
    parser.add_argument("-e", "--epsg", help="reference coordinate system (EPSG number", type=int, default=2056)
    parser.add_argument("-v", "--verbose", action="store_true", help="increase output verbosity")

    args = parser.parse_args()

    # read arguments
    camera_position_file = args.cameras
    DTM_stl_file = args.DTM
    no_of_interpolation_points_x = args.interpol_i
    no_of_interpolation_points_y = args.interpol_j
    output_directory = args.output
    epsg = args.epsg

    sensor_dimension_x = args.sensor_size_x / 1000
    sensor_dimension_y = args.sensor_size_y / 1000
    focal_length = args.focal_lenght / 1000
    sensor_resolution_x = args.sensor_res_x
    sensor_resolution_y = args.sensor_res_y
    flight_height = args.flight_height
    verbose = args.verbose

    process(camera_position_file, DTM_stl_file, no_of_interpolation_points_x, no_of_interpolation_points_y,
            output_directory,
            epsg, sensor_dimension_x, sensor_dimension_y, focal_length, sensor_resolution_x, sensor_resolution_y,
            flight_height, verbose)


if __name__ == "__main__":
    main(sys.argv[1:])

    print(
        "\n================================================================================\nAll photos processed, END\n")
