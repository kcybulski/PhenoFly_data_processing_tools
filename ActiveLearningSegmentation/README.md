&copy; Lukas Roth, Group of crop science, ETH Zurich, Part of: [Phenofly UAS data processing tools](../README.md)

# Image segmentation

Sample scripts to segment RAW image pixels in plant and soil.

## Background
Based on:

Roth et al. 2019 (unpublished)

## Example 

Run ```ActiveLearningSegmentation/_Demo_.py```

**Hint**: If you get an error related to out of memory (e.g. BrokenPipeError: [Errno 32] Broken pipe) then reduce the number of CPUs for parallel processing:
```ActiveLearningSegmentation/Workflow.py```, Line 194, e.g.
```number_of_cpu = 2```

Prerequisit:
1. RAW images 
2. Image masks generated (see [Image mask generation (Standalone Agisoft Script)](../ImageProjectionAgisoft/README.md))


The campaign folder should therefore contain following subfolders:
- ```RAW```
- ```image_masks```


It will generate the following folder:
- ```segmentation```: Segmented images




