from ActiveLearningSegmentation.Workflow import Workflow
from Common import Imagefunctions
import os
from pathlib import Path

base_dir = Path(os.path.join(os.path.dirname(os.path.realpath(__file__))))
if base_dir.name == "ActiveLearningSegmentation":
    base_dir = base_dir / ".."


# Function to extract global sample id
def plot_group_label_func(df):
    return df.plot_label

wfl = Workflow(path_RAW = base_dir / '_TestData/sonyA9/20190402/28m_M600P/RAW',
               raw_extension = ".ARW",
               path_masks = base_dir / '_TestData/sonyA9/20190402/28m_M600P/image_masks',
               path_upload = base_dir / '_TestData/sonyA9/20190402/28m_M600P/segmentation',
               path_workspace = base_dir / '_temp',
               plot_group_label_func = plot_group_label_func,
               minimum_group_size=1,
               sample_type="soil",
               border_pixels=12,
               read_raw_func=Imagefunctions.read_sonyA9_RAW,
               enhanced_raw_func=Imagefunctions.enhance_sonyA9_rawpy,
               preview_func=Imagefunctions.preview_sonyA9_rawpy)

wfl.prepare_workspace()

wfl.prepare_descriptors()

wfl.init_images()

wfl.init_new_classifier()

trainings = ["Training1", "Training2", "Training3", "Training4"]

for training in trainings:
    path_training_data = base_dir / '_TestData' / 'sonyA9' / 'segmentation_training' / (training + '.npy')
    path_training_response_data = base_dir / '_TestData' / 'sonyA9' / 'segmentation_training' / (training + "_response.npy")
    wfl.load_training_data_from_file(path_training_data, path_training_response_data)

wfl.init_sample_masks()


wfl.train_classifier()

#wfl.segment_image_cutouts()
#wfl.GUI()

wfl.segment_images()

wfl.upload_predictions()
